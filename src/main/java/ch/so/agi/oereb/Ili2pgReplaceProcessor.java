package ch.so.agi.oereb;

import org.jboss.logging.Logger;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import ch.ehi.ili2db.base.Ili2db;
import ch.ehi.ili2db.gui.Config;
import ch.ehi.ili2pg.PgMain;
import io.agroal.api.AgroalDataSource;

public class Ili2pgReplaceProcessor implements Processor {
    private static final Logger log = Logger.getLogger(Ili2pgReplaceProcessor.class);
    
    @Override
    public void process(Exchange exchange) throws Exception {
        Object bodyObj = exchange.getIn().getBody();
        File xtfFile;
                        
        // FIXME: Try to create a better route, e.g. conversion with camel.
        if (bodyObj instanceof String) {
            String fileNameOnly = (String) exchange.getProperty("dataset");
            Path path = Paths.get(System.getProperty("java.io.tmpdir"), fileNameOnly);
            byte[] strToBytes = bodyObj.toString().getBytes();
            Files.write(path, strToBytes);
            xtfFile = path.toFile();
        } else {
            xtfFile = (File) bodyObj;
        }
        
        AgroalDataSource dataSource = (AgroalDataSource) exchange.getProperty("datasource");
        
        String url = dataSource.getConnection().getMetaData().getURL();
        String cleanUrl = url.substring(5);
        URI uri = URI.create(cleanUrl);
        String dbhost = uri.getHost();
        String dbport = String.valueOf(uri.getPort());
        String dbdatabase = uri.getPath().substring(1);

        String dbschema = (String) exchange.getProperty("dbschema");
        String dbusr = (String) exchange.getProperty("dbusr");
        String dbpwd = (String) exchange.getProperty("dbpwd");
        String dataset = (String) exchange.getProperty("dataset");

        Config settings = createConfig();

        settings.setFunction(Config.FC_REPLACE);
        settings.setDbhost(dbhost);
        settings.setDbport(dbport);
        settings.setDbdatabase(dbdatabase);
        settings.setDbschema(dbschema);
        settings.setDbusr(dbusr);
        settings.setDbpwd(dbpwd);
        settings.setDatasetName(dataset);
        
        String dburl = "jdbc:postgresql://" + settings.getDbhost() + ":" + settings.getDbport() + "/" + settings.getDbdatabase();
        settings.setDburl(dburl);

        settings.setValidation(false);
        
        if (Ili2db.isItfFilename(xtfFile.getAbsolutePath())) {
            settings.setItfTransferfile(true);
        }
        settings.setXtffile(xtfFile.getAbsolutePath());
                  
        try {
            Ili2db.readSettingsFromDb(settings);
            Ili2db.run(settings, null);            
        } catch (Exception e) {
            log.error("failed to run ili2pg", e);
            log.error(e.getMessage());

            throw new Exception(e);
        }
    }

    private Config createConfig() {
        Config settings = new Config();
        new PgMain().initConfig(settings);
        return settings;
    }
}
