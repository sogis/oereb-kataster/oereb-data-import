package ch.so.agi.oereb;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.zipfile.ZipSplitter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class SecondRoute extends RouteBuilder {

    @ConfigProperty(name = "pathToDownloadFolder") 
    String pathToDownloadFolder;

    @ConfigProperty(name = "pathToUnzipFolder") 
    String pathToUnzipFolder;

    @Override
    public void configure() throws Exception {
        // TODO: try for loop https://stackoverflow.com/questions/21814837/camel-recpient-list-and-file-ftp-polling
        // TODO: use scheduler with cron
        // TODO: file idempotent repository for multiple routes?
        
        /*
        
        // Download zipped federal plr data. 
        from("timer:httpRequestTrigger?repeatCount=1")
        .enrich("https://data.geo.admin.ch/ch.bazl.sicherheitszonenplan.oereb/data.zip")
//      .idempotentConsumer(simple("http-OeREBKRM_V1_1_Codelisten_20170101.xml"), fileConsumerRepository)                        
        .routeId("_download_sicherheitszonenplan_")
        .log(LoggingLevel.INFO, "Downloading: https://data.geo.admin.ch/ch.bazl.sicherheitszonenplan.oereb/data.zip")  
        .setHeader(Exchange.FILE_NAME, simple("ch.bazl.sicherheitszonenplan.oereb.zip"))
        .to("file://"+pathToDownloadFolder);
        
        from("file://"+pathToDownloadFolder+"?noop=true&delay=5000&moveFailed=error")
        .split(new ZipSplitter())
        .streaming().convertBodyTo(String.class) 
            .choice()
                .when(body().isNotNull())
                    .to("file://"+pathToUnzipFolder)
            .end()
        .end();
        
        // Remove silly date appendix from unzipped XTF. 
        from("file://"+pathToUnzipFolder+"?noop=true&delay=5000&moveFailed=error&include=.*\\.xtf")
        // Workaround because substring with simple language with quarkus does not work.
        //.setHeader(Exchange.FILE_NAME, simple("${header.CamelFileName.substring(2,4)}"))        
        .process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                System.out.println(exchange.getIn().getHeader(Exchange.FILE_NAME_ONLY));
                exchange.getIn().setHeader(Exchange.FILE_NAME, "fubar.xtf");
            }

        })
        .to("file://"+pathToDownloadFolder);

        // Import...
        
        */

    }

}
