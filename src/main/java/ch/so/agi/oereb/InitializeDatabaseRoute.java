package ch.so.agi.oereb;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.processor.idempotent.FileIdempotentRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.agroal.api.AgroalDataSource;

/**
 * Lädt drei INTERLIS-Transferdateien herunter und importiert sie gerade anschliessend in die
 * ÖREB-Datenbank. Es handelt sich dabei um die Bundesgesetze, die kantonalen Gesetze und
 * die Bundescodelisten. 
 * 
 * Sie werden nur einmal heruntergeladen und müssen zwingend - da die ÖREB-Daten auf diese 
 * Dateninhalte verweisen - als erstes in die Datenbank importiert werden. Werden die Daten
 * nach einem Reboot der Anwendung nochmals heruntergeladen, werden sie aufgrund des Idempotent Repository
 * nicht ein weiteres Mal importiert. Der Import würde wegen den foreign keys zu Fehlermeldungen
 * führen, falls ÖREB-Daten in der Datenbank vorhanden sind.
 * 
 * @author Stefan Ziegler
 * @since  2019-12-21
 */
@ApplicationScoped
public class InitializeDatabaseRoute extends RouteBuilder {
    
    @ConfigProperty(name = "pathToDownloadFolder") 
    String pathToDownloadFolder;
    
    @ConfigProperty(name = "initializeDatabaseDataFiles") 
    List<String> initializeDatabaseDataFiles;
    
    @ConfigProperty(name = "dbSchema") 
    String dbSchema;

    @ConfigProperty(name = "quarkus.datasource.username") 
    String dbUsr;

    @ConfigProperty(name = "quarkus.datasource.password") 
    String dbPwd;
    
    @Inject
    AgroalDataSource dataSource;

    @Inject
    AppConfig appConfig;
    
    @Inject
    CamelContext context;

    @Override
    public void configure() throws Exception {
        FileIdempotentRepository fileConsumerRepository = appConfig.fileConsumerRepo();

        for(String dataFile : initializeDatabaseDataFiles) {            
            from("timer:httpRequestTriggerInitDatabaseFiles?repeatCount=1")
            .enrich(dataFile)
//            .idempotentConsumer(simple("seda-"+dataFile), fileConsumerRepository)
            .routeId("InitializeDatabase.mergeDownloads."+dataFile)
            .log(LoggingLevel.INFO, "Send to seda: " + dataFile)  
            .process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader(Exchange.FILE_NAME, dataFile.substring(dataFile.lastIndexOf("/")+1));
                }
                
            })
            .to("seda:downloadInitDatabaseFiles");
        }
        
        from("seda:downloadInitDatabaseFiles")
//      .idempotentConsumer(simple("http-ili2pg-${header.CamelFileName}"), fileConsumerRepository)        
        .routeId("InitializeDatabase.downloadFiles")
        .log(LoggingLevel.INFO, "Downloading: ${header.CamelFileName}")
        .streamCaching() // make message re-readable
        .to("file://"+pathToDownloadFolder)
        .setBody(simple("${bodyAs(String)}"))
        .setProperty("datasource", constant(dataSource))
        .setProperty("dbschema", constant(dbSchema))
        .setProperty("dbusr", constant(dbUsr))
        .setProperty("dbpwd", constant(dbPwd))
        .setProperty("dataset", simple("${header.CamelFileName}"))
        .log(LoggingLevel.INFO, "Importing: ${header.CamelFileName}")        
        .process(new Ili2pgReplaceProcessor());
    }
}
