package ch.so.agi.oereb;

import java.net.URI;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.processor.idempotent.FileIdempotentRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.amazonaws.regions.Regions;

import io.agroal.api.AgroalDataSource;

@ApplicationScoped
public class IntegrationRoute extends RouteBuilder {
   
    @ConfigProperty(name = "pathToDownloadFolder") 
    String pathToDownloadFolder;

    @ConfigProperty(name = "pathToErrorFolder") 
    String pathToErrorFolder;

    @ConfigProperty(name = "awsBucketName") 
    String awsBucketName;

    @ConfigProperty(name = "awsAccessKey") 
    String awsAccessKey;

    @ConfigProperty(name = "awsSecretKey") 
    String awsSecretKey;
    
    @ConfigProperty(name = "charset") 
    String charset;

    @ConfigProperty(name = "fileExt") 
    String fileExt;

    @ConfigProperty(name = "dbSchema") 
    String dbSchema;

    @ConfigProperty(name = "quarkus.datasource.username") 
    String dbUsr;

    @ConfigProperty(name = "quarkus.datasource.password") 
    String dbPwd;
  
    @ConfigProperty(name = "downloadDelay") 
    String downloadDelay;

    @ConfigProperty(name = "importDelay") 
    String importDelay;
  
    @ConfigProperty(name = "initialDownloadDelay") 
    String initialDownloadDelay;
            
    @ConfigProperty(name = "initialImportDelay") 
    String initialImportDelay;
  
    @Inject
    AgroalDataSource dataSource;
    
    @Inject
    AppConfig appConfig;
    
    @Override
    public void configure() throws Exception {
        // TODO: send email
        onException(Exception.class)
        .continued(true)
        .log(LoggingLevel.ERROR, simple("${exception.stacktrace}").getText())
        .log(LoggingLevel.ERROR, simple("${exception.message}").getText());

        FileIdempotentRepository fileConsumerRepository = appConfig.fileConsumerRepo();

        // TODO: use for loop
        
        /*
        
        from("timer:httpRequestTrigger?repeatCount=1")
//        .setHeader(Exchange.FILE_NAME, simple("${in.header.CamelAwsS3Key}"))
        .enrich("http://models.geo.admin.ch/V_D/OeREB/OeREBKRM_V1_1_Codelisten_20170101.xml")
        .idempotentConsumer(simple("http-OeREBKRM_V1_1_Codelisten_20170101.xml"), fileConsumerRepository)                        
        .routeId("_XXXXXXdownload_typecodes_")
        .log(LoggingLevel.INFO, "Downloading: http://models.geo.admin.ch/V_D/OeREB/OeREBKRM_V1_1_Codelisten_20170101.xml")  
        .setHeader(Exchange.FILE_NAME, simple("OeREBKRM_V1_1_Codelisten_20170101.xml"))
        .to("file://"+pathToDownloadFolder);
        
        */

        
        // FIXME: regex für verschiedene extensions, (itf,xml,xtf)
        
        /*
        
        // Download from S3
        from("aws-s3://"+awsBucketName+"?accessKey=RAW("+awsAccessKey+")&secretKey=RAW("+awsSecretKey+")&region="+Regions.EU_CENTRAL_1+"&deleteAfterRead=false&autocloseBody=true&maxMessagesPerPoll=60&maxConnections=120&delay="+downloadDelay+"&initialDelay="+initialDownloadDelay)
        .idempotentConsumer(simple("s3-${in.header.CamelAwsS3Key}-${in.header.CamelAwsS3LastModified}"), fileConsumerRepository)                
        .routeId("_download_")
        .log(LoggingLevel.INFO, "Downloading: ${in.header.CamelAwsS3Key}")
        .setHeader(Exchange.FILE_NAME, simple("${in.header.CamelAwsS3Key}"))
        .to("file://"+pathToDownloadFolder);
        
        // Import to database
        // Get credentials from datasource.
        // This does not work for every jdbc driver.
        String url = dataSource.getConnection().getMetaData().getURL();
        String cleanUrl = url.substring(5);
        URI uri = URI.create(cleanUrl);
        String dbHost = uri.getHost();
        String dbPort = String.valueOf(uri.getPort());
        String dbDatabase = uri.getPath().substring(1);

        from("file://"+pathToDownloadFolder+"/?noop=true&charset="+charset+"&include=.*\\."+fileExt+"&maxMessagesPerPoll=120&readLock=changed&delay="+importDelay+"&initialDelay="+initialImportDelay)
        .idempotentConsumer(simple("ili2pg-${header.CamelFileNameOnly}-${header.CamelFileLastModified}"), fileConsumerRepository)                
        .routeId("_ili2pg_")
        .log(LoggingLevel.INFO, "Importing File: ${in.header.CamelFileNameOnly}")
        .setProperty("dbhost", constant(dbHost))
        .setProperty("dbport", constant(dbPort))
        .setProperty("dbdatabase", constant(dbDatabase))
        .setProperty("dbschema", constant(dbSchema))
        .setProperty("dbusr", constant(dbUsr))
        .setProperty("dbpwd", constant(dbPwd))
        .setProperty("dataset", simple("${header.CamelFileName}")) 
        .process(new Ili2pgReplaceProcessor());
        
        */
        
    }
}
