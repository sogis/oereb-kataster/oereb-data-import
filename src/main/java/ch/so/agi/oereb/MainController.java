package ch.so.agi.oereb;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ping")
public class MainController {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
        return "oereb-data-import";
    }
    
    // TODO: Import data on-demand. Use template producer.
}