package ch.so.agi.oereb;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeMainControllerIT extends MainControllerTest {

    // Execute the same tests but in native mode.
}