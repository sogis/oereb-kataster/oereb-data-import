# oereb-data-import


**NOT USED !** Scheint zu kompliziert zu sein, da sehr heterogene Datenflüsse und Abhängigkeiten untereinander. Für den Datenfluss wird GRETL-Jenkins verwendet.

## Developing
```
mkdir -m 0777 ~/oereb-pgdata
docker run --rm --name oereb-db -p 54321:5432 --hostname primary \
-e PG_DATABASE=oereb -e PG_LOCALE=de_CH.UTF-8 -e PG_PRIMARY_PORT=5432 -e PG_MODE=primary \
-e PG_USER=admin -e PG_PASSWORD=admin \
-e PG_PRIMARY_USER=repl -e PG_PRIMARY_PASSWORD=repl \
-e PG_ROOT_PASSWORD=secret \
-e PG_WRITE_USER=gretl -e PG_WRITE_PASSWORD=gretl \
-e PG_READ_USER=ogc_server -e PG_READ_PASSWORD=ogc_server \
-v ~/oereb-pgdata:/pgdata:delegated \
sogis/oereb-db:latest
```

```
docker run --rm --name oereb-db -p 54321:5432 --hostname primary \
-e PG_DATABASE=oereb -e PG_LOCALE=de_CH.UTF-8 -e PG_PRIMARY_PORT=5432 -e PG_MODE=primary \
-e PG_USER=admin -e PG_PASSWORD=admin \
-e PG_PRIMARY_USER=repl -e PG_PRIMARY_PASSWORD=repl \
-e PG_ROOT_PASSWORD=secret \
-e PG_WRITE_USER=gretl -e PG_WRITE_PASSWORD=gretl \
-e PG_READ_USER=ogc_server -e PG_READ_PASSWORD=ogc_server \
sogis/oereb-db:latest
```

## Building

### JVM
```
./mwn clean package -Dmaven.test.skip=true
```

TODO: fix test(s)

```
docker build -f src/main/docker/Dockerfile.jvm -t sogis/oereb-data-import-jvm .
```


### Native Executable
``` 
./mvnw package -Pnative -Dquarkus.native.container-build=true -Dmaven.test.skip=true
```

```
docker build -f src/main/docker/Dockerfile.native -t sogis/oereb-data-import .
```

```
docker build -f src/main/docker/Dockerfile.distroless -t sogis/oereb-data-import .
```
